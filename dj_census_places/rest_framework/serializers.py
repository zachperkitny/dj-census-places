from rest_framework_gis.serializers import GeoModelSerializer

from dj_census_places.models import City, County, State


class StateSerializer(GeoModelSerializer):
    class Meta:
        model = State
        fields = ('name', 'geoid', 'shapefile')
        geo_field = 'shapefile'


class CountySerializer(GeoModelSerializer):
    state = StateSerializer()

    class Meta:
        model = County
        fields = ('name', 'state', 'geoid', 'shapefile',)
        geo_field = 'shapefile'


class CitySerializer(GeoModelSerializer):
    county = CountySerializer()

    class Meta:
        model = City
        fields = ('name', 'county', 'geoid', 'shapefile',)
        geo_field = 'shapefile'
