from django.db import models
from django.contrib.gis.db.models import MultiPolygonField

from polymorphic.models import PolymorphicModel


class Place(PolymorphicModel):
    geoid = models.CharField(max_length=15, unique=True)
    geom = MultiPolygonField() 


class State(Place):
    name = models.TextField()


class County(Place):
    name = models.TextField()
    parent = models.ForeignKey(State, on_delete=models.CASCADE)


class City(Place):
    name = models.TextField()
    parent = models.ForeignKey(County, on_delete=models.CASCADE)
